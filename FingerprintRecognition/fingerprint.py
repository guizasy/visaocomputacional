import cv2
import numpy as np
import logging
import os
from skimage import measure
from skimage.morphology import skeletonize


class FingerPrint(object):
    def __init__(self, save_output=False):
        self.phis = []
        self.dict_minutiae = []
        self.minutiae_extracted = []
        self.thin_images = []
        self.interest_images = []
        self.avg_theta = []
        self.smooth_images = []
        self.dict_core_delta = []
        self.core_delta = []
        self.binary_images = []
        self.images = []
        self.subjects = []
        self.enhancement_images = []
        self.orientation_images = []
        self.alpha = 100
        self.threshold = 100
        self.rows = 150
        self.columns = 150
        self.save_output = save_output
        self.avg_gx = []
        self.avg_gy = []
        self.roi = []
        self.minutiae_list = {
            'isolated': 0,
            'ending': 1,
            'edge_point': 2,
            'bifurcation': 3,
            'crossing': 4
        }

        # create logger
        self.logger = logging.getLogger('FingerPrint')
        self.logger.info('creating an instance of FingerPrint class')

    def load(self, path):
        self.logger.debug('Loading databases path: {}'.format(path))
        self.images = np.array([cv2.imread(i, 0) for i in path])

    def save(self, images, output_path='./output/image/'):
        self.logger.debug("Saving images in: {}".format(output_path))

        if not os.path.exists(output_path):
            os.makedirs(output_path)

        for idx in range(len(images)):
            cv2.imwrite("{}{}.png".format(output_path, self.subjects[idx]), images[idx])

    @staticmethod
    def average_block(gradient, gradient_size=10):
        # Define the window size
        window_size_r = gradient_size
        window_size_c = gradient_size

        mean_blocks = []
        std_blocks = []
        # Crop out the window and calculate the histogram
        for r in range(0, (gradient.shape[0] - window_size_r) + 1, window_size_r):
            for c in range(0, (gradient.shape[1] - window_size_c) + 1, window_size_c):
                window = gradient[r:r + window_size_r, c:c + window_size_c]
                mean_blocks.append(np.average(window))
                std_blocks.append(np.std(window))

        return np.asarray(mean_blocks), np.asarray(std_blocks)

    @staticmethod
    def calc_line(m, block_size):
        half = int(round(block_size / 2.0)) - 1
        m = np.tan(m)

        if np.absolute(m) >= 1:
            y = half
            x = int(round((y / m)))
        else:
            x = half
            y = int(round(m * x))

        return -x, -y, x, y

    def draw_gradients(self, phi, window_size=10):

        window_size_r = window_size
        window_size_c = window_size

        idx = 0
        blank_image = np.ones((self.rows, self.columns), np.uint8) * 255
        for r in range(0, (self.rows - window_size_r) + 1, window_size_r):
            for c in range(0, (self.columns - window_size_c) + 1, window_size_c):

                initial_row = r
                initial_column = c
                final_column = c + window_size_c

                if np.isnan(phi[idx]):
                    cv2.line(blank_image, (initial_column + 1, initial_row + window_size_r / 2),
                             (final_column - 1, initial_row + window_size_r / 2), 0, thickness=1)
                else:
                    x0 = initial_column + window_size_c / 2
                    y0 = initial_row + window_size_r / 2

                    x1, y1, x2, y2 = self.calc_line(phi[idx], window_size)

                    cv2.line(blank_image, (x2 + x0, y2 + y0), (x1 + x0, y1 + y0), 0, thickness=1)

                idx += 1

        return blank_image

    def draw_blocks(self, image, v, window_size=10, threshold=0.8):
        window_size_r = window_size
        window_size_c = window_size

        idx = 0
        masked_image = image.copy()
        for r in range(0, (self.rows - window_size_r) + 1, window_size_r):
            for c in range(0, (self.columns - window_size_c) + 1, window_size_c):
                if v[idx] < threshold:
                    masked_image[r:r + window_size_r, c:c + window_size_c] = 125
                idx += 1

        return masked_image

    def enhancement(self):
        images = self.images

        for idx in range(len(images)):
            mean = np.average(images[idx])
            std = np.std(images[idx])
            abs_img = images[idx]
            if self.threshold > std:
                tmp_img = self.alpha + (self.threshold * (images[idx] - mean) / std)
                cv2.convertScaleAbs(tmp_img, abs_img)

            self.enhancement_images.append(abs_img)

        if self.save_output:
            self.save(self.enhancement_images, './output/enhancement/')

    def orientation(self):
        images = self.enhancement_images

        for idx in range(len(images)):
            median = cv2.medianBlur(images[idx], 5)

            Gx = cv2.Sobel(median, cv2.CV_64F, 1, 0, ksize=3)
            Gy = cv2.Sobel(median, cv2.CV_64F, 0, 1, ksize=3)
            alpha_x = Gx * Gx - Gy * Gy
            alpha_y = 2 * Gx * Gy
            blocks_gx, std = self.average_block(alpha_x, gradient_size=10)
            blocks_gy, std = self.average_block(alpha_y, gradient_size=10)

            self.logger.debug("Block GX Min: {} Max: {}".format(np.amin(blocks_gx), np.amax(blocks_gx)))
            self.logger.debug("Block GY Min: {} Max: {}".format(np.amin(blocks_gy), np.amax(blocks_gy)))

            points = blocks_gy / blocks_gx
            self.arctan_array = np.arctan(points)
            for idx_gradient in range(len(self.arctan_array)):
                if blocks_gx[idx_gradient] < 0 <= blocks_gy[idx_gradient]:
                    self.arctan_array[idx_gradient] += np.pi
                elif blocks_gx[idx_gradient] < 0 and blocks_gy[idx_gradient] < 0:
                    self.arctan_array[idx_gradient] -= np.pi

            phi = (self.arctan_array + np.pi) / 2

            self.phis.append(phi)
            self.interest_images.append(self.interest(images[idx], 10))
            self.avg_gx.append(blocks_gx)
            self.avg_gy.append(blocks_gy)

            if self.save_output:
                orientation = self.draw_gradients(phi)
                tmp = self.images[idx].ravel()
                min = float(np.amin(tmp))
                max = float(np.amax(tmp))
                tmp_img = ((tmp - min) / (max - min))
                tmp_img = tmp_img * 128 + 127
                tmp_img = np.array(tmp_img, dtype=np.uint8)
                tmp_img = tmp_img.reshape(self.rows, self.columns)
                tmp = cv2.bitwise_and(tmp_img, tmp_img, mask=orientation)
                self.orientation_images.append(tmp)

        if self.save_output:
            self.save(self.orientation_images, './output/gradients/')
            self.save(self.interest_images, './output/interest/')

    def calc_distance(self, calc_block, max_dist, gradient_size=10):
        window_size_r = gradient_size
        window_size_c = gradient_size

        origin_x = calc_block.shape[0] / 2
        origin_y = calc_block.shape[1] / 2

        w2 = []
        for r in range(0, (self.rows - window_size_r) + 1, window_size_r):
            for c in range(0, (self.columns - window_size_c) + 1, window_size_c):
                d = np.sqrt(
                    np.power((r + gradient_size / 2) - origin_x, 2) + np.power((c + gradient_size / 2) - origin_y, 2))
                w2.append((max_dist - d) / max_dist)

        return np.array(w2)

    @staticmethod
    def normalize(obj):

        obj_max = np.max(obj)
        obj_min = np.min(obj)

        return (obj - obj_min) / (obj_max - obj_min)

    def interest(self, image, gradient_size=10):
        w0 = 0.5
        w1 = 0.5
        w2max = np.sqrt(np.power(self.columns, 2) + np.power(self.rows, 2)) / 2

        mod = image
        mean_mod, std_mod = self.average_block(mod, gradient_size)
        w2 = self.calc_distance(mod, w2max, gradient_size)

        mean_mod = self.normalize(mean_mod)
        std_mod = self.normalize(std_mod)

        v = w0 * (1 - mean_mod) + w1 * std_mod + w2
        temp = np.full(v.shape[0], True)
        temp[v < 0.8] = False
        self.logger.debug("Interest zone: {}".format(temp))
        self.roi.append(np.asarray(temp))

        return self.draw_blocks(image, v, threshold=0.8)

    def singular_point(self):
        window_size_r = 3
        window_size_c = 3

        for idx in range(len(self.avg_gx)):
            Bx = self.avg_gx[idx].reshape(30, 30)
            By = self.avg_gy[idx].reshape(30, 30)

            angles = np.zeros((30, 30))
            print_angles = np.zeros((30, 30))

            for r in range(0, (Bx.shape[0] - window_size_r) + 1, 1):
                for c in range(0, (By.shape[1] - window_size_c) + 1, 1):
                    window_x = Bx[r:r + window_size_r, c:c + window_size_c]
                    window_y = By[r:r + window_size_r, c:c + window_size_c]

                    a = np.sum(window_x) + window_x[1][1]
                    b = np.sum(window_y) + window_y[1][1]

                    tmp_arctan = np.arctan2(b, a) / 2
                    if not np.isnan(tmp_arctan):
                        angles[r + 1][c + 1] = (tmp_arctan)
                        print_angles[r + 1][c + 1] = angles[r + 1][c + 1] + np.pi / 2

            self.logger.debug("Avg_theta: {} Size: {}".format(angles, len(angles)))
            self.avg_theta.append(np.asarray(angles))
            image = self.draw_gradients(print_angles.ravel())
            self.smooth_images.append(image)

        if self.save_output:
            self.save(self.smooth_images, './output/smooth_gradients/')

    @staticmethod
    def sum_block(window):
        mask = [[0, 0], [1, 0], [2, 0], [2, 1], [2, 2], [1, 2], [0, 2], [0, 1]]

        soma = 0
        for i in range(len(mask)):
            o1 = window[mask[(i + 1) % len(mask)][0]][mask[(i + 1) % len(mask)][1]]
            o2 = window[mask[i][0]][mask[i][1]]
            delta = o2 - o1

            if np.absolute(delta) < (np.pi / 2):
                pass
            elif delta <= -(np.pi / 2):
                delta += np.pi
            else:
                delta = np.pi - delta

            soma += delta

        return soma

    def poincare(self):
        window_size_r = 3
        window_size_c = 3

        self.poincare = []
        counter = 0
        for idx in range(len(self.avg_theta)):
            theta = self.avg_theta[idx]
            interest = self.roi[idx].reshape(30, 30)
            poincare = np.zeros((30, 30))
            for r in range(0, (theta.shape[0] - window_size_r) + 2):
                for c in range(0, (theta.shape[1] - window_size_c) + 2):
                    window_interest = interest[r:r + window_size_r, c:c + window_size_c]
                    if not np.all(window_interest):
                        counter += 1
                        continue

                    window_theta = theta[r:r + window_size_r, c:c + window_size_c]
                    poincare[r + 1][c + 1] = self.sum_block(window_theta) / (2.0 * np.pi)
                    if not (0.45 <= poincare[r + 1][c + 1] <= 0.55 or -0.55 <= poincare[r + 1][c + 1] <= -0.28):
                        poincare[r + 1][c + 1] = 0

            self.logger.debug("Poincare: {}".format(poincare))
            self.poincare.append(np.array(poincare))

        self.logger.debug("Counter: {}".format(counter))
        self.poincare = np.asarray(self.poincare)

    def merge_points(self):
        self.norm_poincare = []
        for idx in range(len(self.poincare)):

            poincare = self.poincare[idx]
            norm_poincare = np.zeros((30, 30))

            ones = poincare.copy()
            ones[ones != 0] = 1
            ones = np.asarray(ones, dtype=np.uint8)

            blobs_labels = measure.label(ones, background=0)

            dict_labels = {}
            for r in range(norm_poincare.shape[0]):
                for c in range(norm_poincare.shape[1]):
                    if blobs_labels[r, c] == 0:
                        continue

                    if 0.45 <= poincare[r][c] <= 0.55:
                        poincare_type = 'c'
                    elif -0.55 <= poincare[r][c] <= -0.28:
                        poincare_type = 'd'

                    if blobs_labels[r, c] in dict_labels:
                        poincare_v, row_v, col_v, type_v = dict_labels[blobs_labels[r, c]]
                        if poincare[r, c] >= 0 and poincare[r, c] > poincare_v or poincare[r, c] < 0 and poincare[
                            r, c] < poincare_v:
                            dict_labels[blobs_labels[r, c]] = (poincare[r, c], r, c, poincare_type)
                    else:
                        dict_labels[blobs_labels[r, c]] = (poincare[r, c], r, c, poincare_type)
                        norm_poincare[r][c] = poincare[r][c]

            self.logger.debug("Dict: {}".format(dict_labels))
            self.dict_core_delta.append(dict_labels)
            self.norm_poincare.append(norm_poincare)

        self.norm_poincare = np.asarray(self.norm_poincare)

    def signature(self):

        for idx in range(len(self.norm_poincare)):

            poincare = self.norm_poincare[idx]
            core = 0
            delta = 0
            image = cv2.cvtColor(self.smooth_images[idx], cv2.COLOR_GRAY2RGB)

            for r in range(len(poincare)):
                for c in range(len(poincare[r])):
                    center_c = c * 10 + 5
                    center_r = r * 10 + 5

                    if 0.45 <= poincare[r][c] <= 0.55:
                        cv2.circle(image, (center_c, center_r), 5, (255, 0, 0), thickness=1)
                        core += 1

                    if -0.55 <= poincare[r][c] <= -0.28:
                        cv2.circle(image, (center_c, center_r), 5, (0, 255, 0), thickness=1)
                        delta += 1

            self.core_delta.append(image)

        if self.save_output:
            self.save(self.core_delta, './output/core_delta/')

    def type_classification(self, dictionary=""):
        dictionary = self.dict_core_delta if dictionary == "" else dictionary
        finger_types = []
        for idx in range(len(dictionary)):
            cores = sum(1 for x in dictionary[idx].values() if x[3] == 'c')
            deltas = sum(1 for x in dictionary[idx].values() if x[3] == 'd')

            self.logger.debug(
                "Value: {} Cores: {} Deltas: {}".format(self.dict_core_delta[idx].values(), cores, deltas))

            if cores == 1 and deltas == 1:
                # Identificar left or right loop
                c = dictionary[idx][1] if dictionary[idx][1][3] == 'c' else dictionary[idx][2]
                d = dictionary[idx][2] if dictionary[idx][2][3] == 'd' else dictionary[idx][1]
                if c[2] < d[2]:
                    fingerprint_type = "Left loop"
                else:
                    fingerprint_type = "Right loop"
            elif cores == 1 and deltas == 0:
                # Identificar arch
                fingerprint_type = "Arch"
            elif cores == 2 or cores == 3 and deltas in [0, 2]:
                # Identificar whorl
                fingerprint_type = "Whorl"
            else:
                # Identificar outros
                fingerprint_type = "Other"

            self.logger.debug("Figure: {} Type: {}".format(self.subjects[idx], fingerprint_type))
            finger_types.append(fingerprint_type)

        return finger_types

    def clean_image(self, img, roi):
        n_img = img.copy()
        window_size_r = 10
        window_size_c = 10
        for r in range(roi.shape[0]):
            for c in range(roi.shape[1]):
                if not roi[r, c]:
                    img_r = r * window_size_r
                    img_c = c * window_size_c
                    n_img[img_r: img_r + window_size_r, img_c: img_c + window_size_c] = 255
        return n_img

    def image_binarization(self):
        dimension = self.columns * self.rows
        block_size = 5

        for idx in range(len(self.enhancement_images)):
            image = self.enhancement_images[idx]
            image = self.clean_image(image, (self.roi[idx]).reshape(30, 30))
            hist, bins = np.histogram(image.ravel(), bins=255, range=[0, 255])

            soma = 0
            p25 = None
            p50 = None

            for i in range(len(hist)):
                soma += hist[i]
                razao = float(soma) / dimension

                if razao >= 0.25 and p25 is None:
                    p25 = i

                if razao >= 0.50 and p50 is None:
                    p50 = i
                    break

            binarized_image = np.full((image.shape[0], image.shape[1]), 255, dtype=np.uint8)
            mean_blocks, std = self.average_block(image, gradient_size=block_size)
            mean_blocks = mean_blocks.reshape(image.shape[0] / block_size, image.shape[1] / block_size)
            for r in range(1, (image.shape[0]) - 1):
                for c in range(1, (image.shape[1]) - 1):
                    if image[r, c] < p25:
                        binarized_image[r, c] = 0
                    elif image[r, c] > p50:
                        binarized_image[r, c] = 255
                    else:
                        block_avg = (np.sum(image[r - 1: r + 2, c - 1: c + 2]) - image[r][c]) / 8
                        br = r / block_size
                        bc = c / block_size
                        binarized_image[r, c] = 255 if block_avg >= mean_blocks[br, bc] else 0

            self.binary_images.append(binarized_image)

        if self.save_output:
            self.save(self.binary_images, './output/binarized/')

    def smoothing(self, image, block_size=5, condition=18):

        smooth_image = image.copy()
        center_point = block_size / 2
        for r in range(self.rows):
            for c in range(self.columns):

                r_start = r - center_point
                c_start = c - center_point
                if r_start < 0:
                    r_start = 0
                if c_start < 0:
                    c_start = 0

                r_end = r + center_point + 1
                c_end = c + center_point + 1
                if r_end > self.rows - 1:
                    r_end = self.rows - 1
                if c_end > self.columns - 1:
                    c_end = self.columns - 1

                window = image[r_start: r_end, c_start: c_end]

                white = np.count_nonzero(window)
                black = block_size * block_size - white

                if white >= condition:
                    smooth_image[r][c] = 255
                elif black >= condition:
                    smooth_image[r][c] = 0

        return smooth_image

    def image_smoothing(self):

        self.smooth_images = []
        for idx in range(len(self.binary_images)):
            image = self.binary_images[idx]

            partial_5 = self.smoothing(image, block_size=5, condition=18)
            partial_3 = self.smoothing(partial_5, block_size=3, condition=5)

            self.smooth_images.append(partial_3)

        if self.save_output:
            self.save(self.smooth_images, './output/smooth/')

    def thinning(self):

        for idx in range(len(self.smooth_images)):
            image = self.smooth_images[idx]
            image[image == 0] = 1
            image[image == 255] = 0
            skeleton = np.asarray(skeletonize(image), dtype=np.uint8)
            skeleton[skeleton == 0] = 255
            skeleton[skeleton == 1] = 0
            self.thin_images.append(skeleton)

        if self.save_output:
            self.save(self.thin_images, './output/skeletonize/')

    def draw_minutiae(self, images, minutiae):
        minutiae_drawn = []

        for idx in range(len(images)):
            img = cv2.cvtColor(images[idx].copy(), cv2.COLOR_GRAY2RGB)

            for r in range(img.shape[0]):
                for c in range(img.shape[1]):
                    if minutiae[idx][r, c] == self.minutiae_list['ending']:
                        cv2.circle(img, (c, r), 1, (0, 0, 255), thickness=1)
                    elif minutiae[idx][r, c] == self.minutiae_list['bifurcation']:
                        cv2.circle(img, (c, r), 1, (0, 255, 0), thickness=1)
                    elif minutiae[idx][r, c] == self.minutiae_list['crossing']:
                        cv2.circle(img, (c, r), 1, (255, 0, 0), thickness=1)

            minutiae_drawn.append(img)

        return np.asarray(minutiae_drawn)

    def minutiae_detection(self):
        self.minutiae_points = []

        for idx in range(len(self.thin_images)):
            image = self.thin_images[idx]
            img_minutiae = np.full(image.shape, 255, dtype=np.uint8)

            for r in range(1, image.shape[0] - 1):
                for c in range(1, image.shape[1] - 1):
                    if image[r, c] == 0:
                        block = image[r - 1: r + 2, c - 1: c + 2]
                        n = len(block[block == 0]) - 1

                        block_minutiae = img_minutiae[r - 3: r + 4, c - 3: c + 4]
                        if n == 0 and len(block_minutiae[block_minutiae == self.minutiae_list['isolated']]) == 0:
                            img_minutiae[r, c] = self.minutiae_list['isolated']
                        elif n == 1 and len(block_minutiae[block_minutiae == self.minutiae_list['ending']]) == 0:
                            img_minutiae[r, c] = self.minutiae_list['ending']
                        elif n == 2 and len(block_minutiae[block_minutiae == self.minutiae_list['edge_point']]) == 0:
                            img_minutiae[r, c] = self.minutiae_list['edge_point']
                        elif n == 3 and len(block_minutiae[block_minutiae == self.minutiae_list['bifurcation']]) == 0:
                            img_minutiae[r, c] = self.minutiae_list['bifurcation']
                        elif n >= 5 and len(block_minutiae[block_minutiae == self.minutiae_list['crossing']]) == 0:
                            img_minutiae[r, c] = self.minutiae_list['crossing']

            self.minutiae_points.append(img_minutiae)

        if self.save_output:
            minutiae_drawn = self.draw_minutiae(self.thin_images, self.minutiae_points)
            self.save(minutiae_drawn, './output/minutiae_detection/')

    def check_endings(self, image, r, c, roi, block_size=10):
        block_row = r / block_size
        block_columns = c / block_size
        mask = [[block_row - 1, block_columns - 1], [block_row, block_columns - 1],
                [block_row + 1, block_columns - 1], [block_row + 1, block_columns],
                [block_row + 1, block_columns + 1], [block_row, block_columns + 1],
                [block_row - 1, block_columns + 1], [block_row - 1, block_columns]]

        for i in range(len(mask)):
            index_r = mask[i][0]
            index_c = mask[i][1]

            if index_r < 0 or index_c < 0 or index_r >= roi.shape[0] or index_c >= roi.shape[1]:
                continue

            if not roi[index_r][index_c]:
                image[r][c] = 255
                break

        return image

    def remove_spurious(self, image, final_image, r_start, r_end, c_start, c_end, r, c):
        ret_window = final_image.copy()
        blk_img = image[r_start:r_end, c_start:c_end]

        qtd_ending = len(blk_img[blk_img == self.minutiae_list['ending']])
        qtd_bifurcation = len(blk_img[blk_img == self.minutiae_list['bifurcation']])

        if image[r, c] == self.minutiae_list['ending'] and qtd_ending > 1:
            ret_window[r, c] = 255
        if image[r, c] == self.minutiae_list['bifurcation'] and qtd_bifurcation > 1:
            ret_window[r, c] = 255
        if image[r, c] == self.minutiae_list['ending'] and qtd_bifurcation > 0 or image[r, c] == self.minutiae_list['bifurcation'] and qtd_ending > 0:
            ret_window[r, c] = 255
        if image[r, c] == self.minutiae_list['crossing'] and qtd_ending > 0:
            ret_window[r, c] = 255

        return ret_window

    def minutiae_extraction(self):
        block_size = 15
        center_point = block_size / 2

        for idx in range(len(self.minutiae_points)):
            image = self.minutiae_points[idx]
            final_image = self.minutiae_points[idx].copy()

            for r in range(image.shape[0] - 1):
                for c in range(image.shape[1] - 1):
                    if image[r][c] == 255 or image[r][c] == self.minutiae_list['edge_point'] or image[r][c] == self.minutiae_list['isolated']:
                        continue

                    r_start = r - center_point
                    c_start = c - center_point
                    if r_start < 0:
                        r_start = 0
                    if c_start < 0:
                        c_start = 0

                    r_end = r + center_point + 1
                    c_end = c + center_point + 1
                    if r_end > self.rows - 1:
                        r_end = self.rows - 1
                    if c_end > self.columns - 1:
                        c_end = self.columns - 1

                    final_image = self.check_endings(final_image, r, c, (self.roi[idx]).reshape(30, 30))
                    final_image = self.remove_spurious(image, final_image, r_start, r_end, c_start, c_end, r, c)

            self.minutiae_extracted.append(final_image)

        if self.save_output:
            minutiae_drawn = self.draw_minutiae(self.thin_images, self.minutiae_extracted)
            self.save(minutiae_drawn, './output/minutiae_extraction/')

    def norm_minutiae(self, block_size=10):
        for idx in range(len(self.minutiae_extracted)):
            min_val = 3000
            key = None
            image = self.minutiae_extracted[idx]

            for k in self.dict_core_delta[idx].keys():
                (_, r, c, type) = self.dict_core_delta[idx][k]
                if (key is None or r < min_val) and type == 'c':
                    key = k
                    min_val = r

            (_, r, c, type) = self.dict_core_delta[idx][key]
            alpha_array = self.phis[idx].reshape(30, 30)
            alpha = alpha_array[r][c]
            r0 = r * block_size + 5
            c0 = c * block_size + 5

            dict_minutiae = {}
            count = 0
            for row in range(image.shape[0]):
                for column in range(image.shape[1]):
                    if image[row][column] == 255 or image[row][column] == 0 or image[row][column] == 2:
                        continue

                    row0 = row - r0
                    column0 = column - c0

                    # y '=x\sin \theta +y\cos \theta \,'
                    # row_rot = - column0 * np.sin(alpha) + row0 * np.cos(alpha)
                    # x '=x\cos \theta -y\sin \theta \,
                    # column_rot = column0 * np.cos(alpha) + row0 * np.sin(alpha)

                    # dict_minutiae[count] = (row_rot, column_rot, image[row][column])
                    dict_minutiae[count] = (row0, column0, image[row][column])
                    count += 1

            self.dict_minutiae.append(dict_minutiae)

    def compute_score(self, idx, idx_comp):
        interest_radius = 40
        score = 0
        for idx_sec in range(len(self.dict_minutiae[idx])):
            min_value = 9999
            (rm, cm, type_m) = self.dict_minutiae[idx][idx_sec]
            for idx_comp_sec in self.dict_minutiae[idx_comp]:
                (rc, cc, type_c) = self.dict_minutiae[idx_comp][idx_comp_sec]

                if type_m != type_c:
                    continue

                value = np.linalg.norm([(rm - rc), (cm - cc)])
                min_value = value if value <= min_value else min_value

            if min_value < interest_radius:
                score += (1 - (min_value / interest_radius))

        return score * 100 / len(self.dict_minutiae[idx])

    def match_name(self, idx, idx_match):
        basename_orig = self.subjects[idx][1:4]
        basename_comp = self.subjects[idx_match][1:4]

        self.logger.debug("Base: {} Comp: {}".format(basename_orig, basename_comp))

        return basename_orig == basename_comp

    def matching(self):
        self.norm_minutiae()
        base = self.type_classification()
        avg_score = 0
        for idx in range(len(self.dict_minutiae)):
            max_score = 0
            idx_max = -1
            for idx_comp in range(len(self.dict_minutiae)):
                if idx == idx_comp or base[idx] != base[idx_comp]:
                    continue

                score = self.compute_score(idx, idx_comp)
                if score > max_score:
                    max_score = score
                    idx_max = idx_comp

            if self.match_name(idx, idx_max):
                avg_score += 1

        print("\nAvg Score predicting subject: {}".format((1.0 * avg_score) / len(self.dict_minutiae)))
