#!/usr/bin/env python

import argparse
import logging
import sys

from tsinghua import TsingHua

_log_level = {
    'DEBUG': logging.DEBUG,
    'ERROR': logging.ERROR,
    'WARNING': logging.WARNING,
    'INFO': logging.INFO
}

def configure_logging(args):
    # create logger with 'fingerprint_application'
    root = logging.getLogger()
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    root.setLevel(_log_level[args.log_level])

    # create file handler which logs even debug messages
    if args.output_log_file != '':
        fh = logging.FileHandler(args.output_log_file, 'w')
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        root.addHandler(fh)

    # create console handler with a higher log level
    if not args.quiet:
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)
        root.addHandler(ch)

def main(argv):
    parser = argparse.ArgumentParser('python run.py ')
    parser.add_argument('--db_name', '-d', dest='dbname', help='database images to be used', default='Rindex28')
    parser.add_argument('--save_output', '-s', action='store_true', help='save output images of process')
    parser.add_argument('--log_level', '-l', dest='log_level', help='set log level of application', default='ERROR')
    parser.add_argument('--output_log', '-o', dest='output_log_file', help='output file to be used', default='')
    parser.add_argument("--quiet", "-q", action="store_true", help='quiet console output')
    args = parser.parse_args()

    configure_logging(args)
    logging.debug('Arguments received: {}'.format(args))
    logging.info('creating an instance of Main function')
    finger = None if args.dbname == "FVC" else TsingHua(args.save_output)

    if finger is not None:
        finger.load_database()
        # finger.load_database(path='./databases/*/f001R1.raw')
        # finger.load_database(path='./databases/*/f001*.raw')
        # finger.load_database(path='./databases/*/f00*.raw')
        # finger.load_database(path='./databases/*/f002R1.raw')
        # finger.load_database(path='./databases/*/f005R1.raw')
        finger.load_json()
        finger.classify()

if __name__ == '__main__':
    main(sys.argv[1:])