import fingerprint
import glob
import logging
import numpy as np
import re
import json, os


class TsingHua(fingerprint.FingerPrint):
    def __init__(self, save_output=False):
        super(TsingHua, self).__init__()
        self.alpha = 150
        self.threshold = 95
        self.rows = 300
        self.columns = 300
        self.save_output = save_output
        self.json_dictionary = []

        # create logger
        self.logger = logging.getLogger('TsingHua')
        self.logger.info('creating an instance of TsingHau class')

        np.set_printoptions(precision=4, linewidth=200, suppress=True)

    def load(self, path):
        self.images = [np.fromfile(i, dtype='uint8', sep="").reshape(300, 300) for i in path]

    def load_database(self, path="./databases/*/*.raw"):
        images_path = glob.glob(path)

        self.logger.info('Images path: {}'.format(path))

        self.subjects = []
        for path in images_path:
            partial = re.sub(r".*Rindex28\\", "", path)
            final = re.sub(r".raw", "", partial)
            self.subjects.append(final)

        self.load(images_path)

    def load_json(self, path="./databases/*/*.lif"):
        files = glob.glob(path)
        self.json_dictionary = []
        for f in files:

            file_data = open(f).read()
            data = json.loads(file_data)

            dict_labels = {}
            for es, s in enumerate(data["shapes"]):
                sig_type = 'c' if s["label"] == "Core" or s["label"] == "core" else 'd'
                dict_labels[es + 1] = (0, s["points"][0][1], s["points"][0][0], sig_type)
            self.json_dictionary.append(dict_labels)

    def print_type_classification(self, base, label=None):

        if label is None:
            for idx in range(len(self.subjects)):
                print("Figure: {} Type: {}".format(self.subjects[idx], base[idx]))
        else:
            count = 0
            for idx in range(len(self.subjects)):
                match = 'false'
                if base[idx] == label[idx]:
                    match = 'true'
                    count += 1
                print(
                    "Figure: {: <12} Base Type: {: <12} Label Type: {: <12} Match: {}".format(self.subjects[idx],
                                                                                              base[idx],
                                                                                              label[idx], match))
            print("\nAvg_Score: {}".format(float(count) / len(self.subjects)))

    def classify(self):
        self.enhancement()
        self.orientation()
        self.singular_point()
        self.poincare()
        self.merge_points()
        self.signature()
        base = self.type_classification()
        if self.json_dictionary:
            label = self.type_classification(self.json_dictionary)
            self.print_type_classification(base, label)
        else:
            self.print_type_classification(base)

        self.image_binarization()
        self.image_smoothing()
        self.thinning()
        self.minutiae_detection()
        self.minutiae_extraction()
        self.matching()
