import glob
import itertools

import numpy as np
from sklearn.cross_validation import LeaveOneOut

import cv2
from FaceProcess import ImgProcess


class YaleFaces(ImgProcess):

    def __init__(self, path='./databases/yalefaces/', num_components=10):
        super(YaleFaces, self).__init__(num_components)
        self._path = path
        self._rule = 'subject*'
        self._columns = 320
        self._rows = 243
        self.groups = ['.centerlight', '.glasses', '.happy', '.leftlight', '.noglasses',
                       '.normal', '.rightlight', '.sad', '.sleepy', '.surprised', '.wink']

    def generate_files(self):
        imgs_path = glob.glob(self._path + self._rule)
        self.imgs = self.load_images(imgs_path)
        self.eigen_face()
        self.print_eigen('yale_eigen_')

    def create_folds(self):
        self.folds = []
        for i in self.groups:
            tmp = glob.glob('{0}/*{1}'.format(self._path, i))
            self.folds.append(tmp)

    def merge_folds(self, X):
        return list(itertools.chain.from_iterable(X))

    def cross_validation(self):
        print "Starting leave-one-out cross validation process\n"
        self.loo = LeaveOneOut(len(self.groups))
        self.create_folds()

        amean = []
        internal_fold = np.array(self.folds)
        for train_index, test_index in self.loo:
            X_train, X_test = internal_fold[train_index], internal_fold[test_index]
            X_train = self.merge_folds(X_train)
            self.imgs = self.load_images(X_train)
            self.guess = self.load_images(X_test[0])
            self.eigen_face()

            mean = 0
            for i in range(len(self.guess)):
                idx = self.recognise(self.guess[i])
                real_subject = (X_test[0][i]).split('/')[3].split('.')[0]
                predicted = (X_train[idx]).split('/')[3].split('.')[0]
                mean += 1 if real_subject == predicted else 0
            mean = mean / float(len(self.guess))
            print "Processing {0} with acurracy: {1}".format((X_test[0][i]).split('/')[3].split('.')[1], mean)
            amean.append(mean)

        print "\nThe worst class in recognition process was {0} of class {1}".format(min(amean), self.groups[amean.index(min(amean))].split('.')[1])
        amean = np.array(amean)
        print "\nMEAN: {0}  STD_DEV: {1}".format(np.mean(amean), np.std(amean))
