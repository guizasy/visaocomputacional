import glob
import os

import matplotlib.pyplot as plt
import numpy as np
from scipy import linalg
from scipy.misc import *

imgs_path = glob.glob('../databases/att_faces/s*/*.pgm')
# imgas = np.array([imread(i, True).flatten() for i in imgs_path])
imgs = np.array([imread(i, True).flatten() for i in imgs_path], dtype='uint8')
#imshow(np.array(imgs[0], dtype=np.uint8).reshape(112, 92))

# imgs = imgas.astype(np.uint8)
mean = np.mean(imgs, 0)
# imshow(np.array(mean, dtype=np.uint8).reshape(112, 92))
# plt.imsave('my_mean.jpg', np.array(mean, dtype=np.uint8).reshape(112, 92), cmap='Greys_r')
dimgs = imgs - mean
# mcov = np.cov(dimgs)
mcov = np.dot(dimgs, dimgs.T)
mevals, mevects = linalg.eig(mcov)
# mevals = np.real(mevals)
# mevects = np.real(mevects)

# idx = mevals.argsort()[::-1]
# mevals = mevals[idx]
# eigenVectors = mevects[:,idx]

eord = np.argsort(mevals)
eord[:] = eord[::-1]  # index of sorted eigenvalues

# [K,1] = [K,nImg].[nImg,K]
# eigenFaces = np.dot(dimgs, mevects[:, eord[range(5)]])

eigenFaces = np.dot(mevects[:, eord[range(5)]].T, dimgs)

x = imgs[0]
dimgs_t = dimgs[1::].T
# proj = eigenFaces * (x - mean)
proj = np.dot(eigenFaces, (x-mean))
proj_k = np.dot(eigenFaces, dimgs_t)

e_2 = (proj.T - proj_k.T)
e_2 = np.power(e_2, 2)
# eigenFaces = np.dot(eigenVectors[range(5), :], dimgs)

# eigenFaces = []
# # for i in range(len(dimgs)):
# for i in range(5):
#     # eigenFace = np.dot(eigenVectors[:, i], dimgs)
#     eigenFace = np.dot(dimgs, eigenVectors[:, i])
#     eigenFaces.append(eigenFace)

    # print 'shape: {0}'.format(imgs.shape)
    # print 'shape: {0}'.format(evects.shape)
    # print 'shape: {0}/{1}'.format(eigenFace.shape,eigenFace.dtype)


# eigenFaces = (eigenFaces - np.amin(eigenFaces)) / (np.amax(eigenFaces) - np.amin(eigenFaces))
# eigenFaces = np.array(255 * eigenFaces, dtype=np.uint8)
# eigenFaces = eigenFaces.reshape(5, 112, 92)
#
# ims = None  # for exhibition
# for i in range(5):
#     if ims is None:
#         ims = plt.imshow(eigenFaces[i], cmap='Greys_r')
#     else:
#         ims.set_data(eigenFaces[i])
#
#     plt.imsave('my_eigen{0}.jpg'.format(i), eigenFaces[i], cmap='Greys_r');
#     plt.pause(2)
#     plt.draw()


## Classifier
