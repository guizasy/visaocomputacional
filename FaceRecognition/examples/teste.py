import glob
import os

import matplotlib.pyplot as plt
import numpy as np
from scipy import linalg
from scipy.misc import *

imgs_path = glob.glob('../databases/att_faces/s*/*.pgm')
# imgas = np.array([imread(i, True).flatten() for i in imgs_path])
imgs = np.array([imread(i, True).flatten() for i in imgs_path], dtype='uint8')
#imshow(np.array(imgs[0], dtype=np.uint8).reshape(112, 92))

# imgs = imgas.astype(np.uint8)
mean = np.mean(imgs, 0)
# imshow(np.array(mean, dtype=np.uint8).reshape(112, 92))
# plt.imsave('my_mean.jpg', np.array(mean, dtype=np.uint8).reshape(112, 92), cmap='Greys_r')
dimgs = imgs - mean
mcov = np.cov(dimgs)
evals, evects = linalg.eig(mcov)

ims = None  # for exhibition
eigenFaces = []
for i in range(len(dimgs)):
    eigenFace = np.dot(evects[i, :], dimgs)
    eigenFace = np.array(eigenFace, dtype=np.uint8)#.reshape(112, 92)
    eigenFaces.append(eigenFace)

    print 'shape: {0}'.format(imgs.shape)
    print 'shape: {0}'.format(evects.shape)
    print 'shape: {0}/{1}'.format(eigenFace.shape,eigenFace.dtype)

    if ims is None:
        ims = plt.imshow(eigenFace, cmap='Greys_r')
    else:
        ims.set_data(eigenFace)

    plt.imsave('my_eigen{0}.jpg'.format(i), eigenFace, cmap='Greys_r');
    plt.pause(2)
    plt.draw()
