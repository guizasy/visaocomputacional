#!/usr/bin/python

import os

import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as npla

# Import the required modules
import cv2
#import matplotlib.image as mpimg
from PIL import Image


class FaceId:

    DBPath = dict(yale='/home/menotti/databases/yalefaces/',
                  orl='/home/menotti/databases/orl_faces/')

    nEf = 0

    _path = ''  # virtual path

    def __init__(self, path=_path):
        self.path = path
        self.get_images_and_labels(self.path)

    def meanFace(self):
        imgmf = np.zeros(self.images[0].shape, dtype=np.uint32)  # due to integer summations uint32
        for im in self.images:
            imgmf = imgmf + im
        imgmf = imgmf / len(self.images)
        imgmf = np.array(imgmf, dtype=np.uint8)  # converting back to uin8

        return imgmf

    def meanFace2(self):
        mMF = np.average(np.array(self.images), axis=0)  # computes the average face
        mMF = np.array(mMF, dtype=np.uint8)

        return mMF

    def eigenFaces(self):
        # covariance matrix
        nImg = len(self.images)
        aimages = np.asarray(self.images).T
        aimages = np.reshape(np.ravel(aimages), (-1, nImg))

        # to be done!

    def eigenFaces2(self, _nEf):
        self.nEf = _nEf

        nImg = len(self.images)
        iH, iW = self.images[0].shape
        K = iH * iW
        print 'nImg: {0}, iH: {1}, iW: {2}, K: {3}'.format(nImg, iH, iW, K)

        mI = np.ravel(np.asarray(self.images)).reshape(nImg, -1).T  # [KxnImg], K = WxH
        K, nImg = mI.shape
        aM = np.average(mI, axis=1).reshape(-1, 1)  # [K,1]
        mM = np.tile(aM, (1, nImg))  # [K,nImg]
        mA = mI - mM  # [K,nImg]
        mC = np.dot(mA.T, mA)  # [nImg,nImg] = [nImg,K].[K,nImg]
        evals, evects = npla.eig(mC)  # [nImg,nV] -- A^T A
        evects = np.real(evects)
        eord = np.argsort(evals)
        eord[:] = eord[::-1]  # index of sorted eigenvalues

        # [K,1] = [K,nImg].[nImg,K]
        mU = np.dot(mA, evects[:, eord[range(self.nEf)]])
        print 'mU.shape:{0}, mA.shape:{1}, evects:{2}'.format(mU.shape, mA.shape, evects[:, eord[range(self.nEf)]].shape)

        return mU

    def eigenFaces2Img(self, efaces):
        nImg = len(self.images)
        iH, iW = self.images[0].shape
        K = iH * iW
        print 'nImg: {0}, iH: {1}, iW: {2}, K: {3}'.format(nImg, iH, iW, K)
        efaces = (efaces - np.amin(efaces)) / (np.amax(efaces) - np.amin(efaces))
        efaces = np.array(255 * efaces, dtype=np.uint8)
        efaces = efaces.T.reshape(self.nEf, iH, iW)

        return efaces


class ORLFaces(FaceId):

    _path = './orl_faces'

    def get_images_and_labels(self, path=_path):
        # images will contains face images
        self.images = []
        # subjets will contains the subject identification number assigned to the image
        self.subjects = []

        subjects_paths = [os.path.join(path, d) for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]
        for s, subject_paths in enumerate(subjects_paths, start=1):

            nbr = int(os.path.split(subject_paths)[1].split(".")[0].replace("s", ""))
            print 'sub: {0}--{1}'.format(subject_paths, nbr)

            subject_path = [os.path.join(subject_paths, f) for f in os.listdir(
                subject_paths) if f.endswith('.pgm') and os.path.isfile(os.path.join(subject_paths, f))]

            for image_path in subject_path:
                #				print 'img: {0}'.format(image_path)
                # Read the image and convert to grayscale
                image_pil = Image.open(image_path).convert('L')
                # Convert the image format into numpy array
                image = np.array(image_pil, 'uint8')
                # Get the label of the image
                self.images.append(image)
                self.subjects.append(nbr)

#			print 'sub: {0}({1}#) - {2}'.format(s,len(subject_path),subject_paths)


class YaleFaces(FaceId):

    _path = './yalefaces'
    # classes: center-light, w/glasses, happy, left-light, w/no glasses,
    # normal, right-light, sad, sleepy, surprised, and wink.
    class_labels = ['.centerlight', '.glasses', '.happy', '.leftlight', '.noglasses',
                    '.normal', '.rightlight', '.sad', '.sleepy', '.surprised', '.wink']
    # Note that the image "subject04.sad" has been corrupted and has been substituted by "subject04.normal".
    # Note that the image "subject01.gif" corresponds to
    # "subject01.centerlight" :~ mv subject01.gif subject01.centerlight

    def get_images_and_labels(self, path=_path):
        # Append all the absolute image paths in a list image_paths
        # We will not read the image with the .sad extension in the training set
        # Rather, we will use them to test our accuracy of the training

        # images will contains face images
        self.images = []
        # subjets will contains the subject identification number assigned to the image
        self.subjects = []
        # classes
        self.classes = []

        for c, class_label in enumerate(self.class_labels, start=1):
            image_paths = [os.path.join(path, f) for f in os.listdir(path) if f.endswith(class_label)]

            for image_path in image_paths:
                #				print 'Image: ' + image_path
                # Read the image and convert to grayscale
                image_pil = Image.open(image_path).convert('L')
                # Convert the image format into numpy array
                image = np.array(image_pil, 'uint8')
                # Get the label of the image
                nbr = int(os.path.split(image_path)[1].split(".")[0].replace("subject", ""))

                self.images.append(image)
                self.subjects.append(nbr)
                self.classes.append(class_label)

#			print 'class_label: {0}({1}#) - {2}'.format(c,len(image_paths), class_label)

# Path to the Yale Dataset
#path = '/home/menotti/databases/yalefaces/'
# print 'loading Yalefaces database'
#yale = YaleFaces(path)
# yale.eigenFaces2()

# Path to the ORl Dataset
path = '../databases/att_faces/'
print 'loading ORL database'
orl = ORLFaces(path)
mu = orl.eigenFaces2(5)
imgs = orl.eigenFaces2Img(mu)

for i in range(len(imgs)):
    plt.imsave('eigen_{0}.jpg'.format(i), imgs[i], cmap='Greys_r');
