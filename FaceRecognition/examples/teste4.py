import os

import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as npla

# Import the required modules
import cv2
#import matplotlib.image as mpimg
from PIL import Image


def get_images_and_labels(path, images, subjects):
    # images will contains face images
    # images = []
    # subjets will contains the subject identification number assigned to the image
    # subjects = []

    subjects_paths = [os.path.join(path, d) for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]
    for s, subject_paths in enumerate(subjects_paths, start=1):
        subject_path = [os.path.join(subject_paths, f) for f in os.listdir(subject_paths) if f.endswith('.pgm') and os.path.isfile(os.path.join(subject_paths, f))]

        for image_path in subject_path:
            # print 'sub: {0}'.format(image_path)
            # Read the image and convert to grayscale
            image_pil = Image.open(image_path).convert('L')
            # Convert the image format into numpy array
            image = np.array(image_pil, 'uint8')
            # Get the label of the image
            nbr = int(os.path.split(image_path)[1].split(".")[0])

            images.append(image)
            subjects.append(nbr)

imgs_2 = []
subjects_2 = []
# get_images_and_labels('../databases/yale/', imgs_2, subjects_2)
get_images_and_labels('../databases/att_faces/', imgs_2, subjects_2)

nImg = len(imgs_2)
iH, iW = imgs_2[0].shape
K = iH * iW
print 'nImg: {0}, iH: {1}, iW: {2}, K: {3}'.format(nImg, iH, iW, K)

mI = np.ravel(np.asarray(imgs_2)).reshape(nImg, -1).T  # [KxnImg], K = WxH
K, nImg = mI.shape
aM = np.average(mI, axis=1).reshape(-1, 1)  # [K,1]
mM = np.tile(aM, (1, nImg))  # [K,nImg]
mA = mI - mM  # [K,nImg]
mC = np.dot(mA.T, mA)  # [nImg,nImg] = [nImg,K].[K,nImg]
evals, evects = npla.eig(mC)  # [nImg,nV] -- A^T A
evects = np.real(evects)
eord = np.argsort(evals)
eord[:] = eord[::-1]  # index of sorted eigenvalues

# [K,1] = [K,nImg].[nImg,K]
mU = np.dot(mA, evects[:, eord[range(5)]])
