import glob
import itertools

from sklearn.cross_validation import KFold
import numpy as np

import cv2
from FaceProcess import ImgProcess


class ORLFaces(ImgProcess):

    def __init__(self, path='./databases/att_faces/', num_components = 10):
        super(ORLFaces, self).__init__(num_components)
        self._path = path
        self._rule = 's*/*.pgm'
        self._columns = 92
        self._rows = 112

    def generate_files(self):
        imgs_path = glob.glob(self._path + self._rule)
        self.imgs = self.load_images(imgs_path)
        self.eigen_face()
        self.print_eigen('orl_eigen_')

    def create_folds(self):
        self.folds = []
        for i in range(10):
            tmp = glob.glob('./databases/att_faces/s*/{0}.pgm'.format(i + 1))
            self.folds.append(tmp)

    def merge_folds(self, X):
        return list(itertools.chain.from_iterable(X))

    def cross_validation(self):
        print "Starting Kfold cross validation process"
        self.kf = KFold(10, n_folds=10)
        self.create_folds()

        amean = []
        internal_fold = np.array(self.folds)
        for train_index, test_index in self.kf:
            X_train, X_test = internal_fold[train_index], internal_fold[test_index]
            X_train = self.merge_folds(X_train)

            self.imgs = self.load_images(X_train)
            self.guess = self.load_images(X_test[0])
            self.eigen_face()

            mean = 0
            for i in range(len(self.guess)):
                idx = self.recognise(self.guess[i])
                real_subject = (X_test[0][i]).split('/')[3]
                predicted = (X_train[idx]).split('/')[3]
                if real_subject == predicted:
                    mean = mean + 1
            mean = mean / float(len(self.guess))
            amean.append(mean)
        amean = np.array(amean)
        print "\nMEAN: {0}  STD_DEV: {1}".format(np.mean(amean), np.std(amean))
