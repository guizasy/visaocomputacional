import glob
import os

import matplotlib.pyplot as plt
import numpy as np
from scipy import linalg
from scipy.misc import *

class ImgProcess(object):

    def __init__(self, num_components = 10):
        self.evals = []
        self.evects = []
        self.num_components = num_components
        self._columns = 100
        self._rows = 100

    def load_images(self, imgs_list_path):
        return np.array([imread(i, True).flatten() for i in imgs_list_path])

    def eigen_face(self):
        nFe = self.num_components
        [n, d] = self.imgs.shape
        self.mean = np.mean(self.imgs, 0)
        self.dimgs = self.imgs - self.mean

        if n > d:
            mcov = np.dot(self.dimgs.T, self.dimgs)
            [self.evals, self.evects] = linalg.eigh(mcov)
        else:
            mcov = np.dot(self.dimgs, self.dimgs.T)
            [self.evals, self.evects] = linalg.eigh(mcov)
            self.evects = np.dot(self.dimgs.T, self.evects)
            for i in xrange(n):
                self.evects[:, i] = self.evects[:, i] / np.linalg.norm(self.evects[:, i])

        idx = np.argsort(-self.evals)
        self.evals = self.evals[idx]
        self.evects = self.evects[:, idx]

        self.evals = self.evals[0:nFe].copy()
        self.evects = self.evects[:, 0:nFe].copy()

    def normalize(self, X):
        X = 255 * ((X - np.amin(X)) / (np.amax(X)-np.amin(X)))
        return np.asarray(X)

    def print_eigen(self, prefix):
        nFe = self.num_components
        for i in range(nFe):
            eigenFaces = self.evects.T[i]
            eigenFaces = self.normalize(eigenFaces)
            plt.imsave('{0}{1}.jpg'.format(prefix, i), eigenFaces.reshape(self._rows, self._columns), cmap='Greys_r');

    def reconstruct(self, prefix, nFe):
        P = np.dot(self.imgs[0].reshape(1, -1) - self.mean, self.evects[:, 0:nFe])
        R = np.dot(P, self.evects[:, 0:nFe].T) + self.mean
        R = R.reshape(self.imgs[0].shape)
        print "Shape_R: {0}".format(R.shape)
        normalized = self.normalize(R)
        plt.imsave("{0}.jpg".format(prefix), normalized.reshape(self._rows, self._columns), cmap='Greys_r')

    def recognise(self, img):
        PK = np.dot(img - self.mean, self.evects)
        P = np.dot(self.dimgs, self.evects)

        Dist = []
        for i in range(len(P)):
            d = linalg.norm(PK - P[i])
            Dist.append(d)

        return Dist.index(min(Dist))
