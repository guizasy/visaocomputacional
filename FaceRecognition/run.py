#!/usr/bin/env python

import getopt
import sys

from ORL import ORLFaces
from YaleFace import YaleFaces

def main(argv):

    try:
        opts, args = getopt.getopt(argv, "d:n:cp")
    except getopt.GetoptError:
        print 'Valid options: -d <yale | orl> -n <# EigenFaces> -c -p'
        sys.exit(1)

    # default
    database = 'yale'
    num_components = 10
    cross_validation = False
    print_eigen = False

    for opt, arg in opts:
        if opt == '-d':
            database = arg
        if opt == '-n':
            num_components = int(arg)
        if opt == '-c':
            cross_validation = True
        if opt == '-p':
            print_eigen = True

    print 'Creating class instance {0} ...'.format(database)
    db = YaleFaces(num_components=num_components) if database == 'yale' else ORLFaces(num_components=num_components)

    if print_eigen:
        print "Using all images in database to generate {0} eigenfaces".format(num_components)
        db.generate_files()

    if cross_validation:
        print "Invoking Cross Validation processor"
        db.cross_validation()

if __name__ == '__main__':
    main(sys.argv[1:])
