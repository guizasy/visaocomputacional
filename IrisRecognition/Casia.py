import IrisBase
import glob
import numpy as np
import cv2
import re


class CasiaDB(IrisBase.IrisBase):
    def __init__(self):
        super(CasiaDB, self).__init__()
        self._columns = 640
        self._rows = 480

    def load_base(self, path='./databases/casia', rule='/*/L/*.jpg'):
        iris_path = glob.glob(path + rule)

        for path in iris_path:
            partial = re.sub(r".*S2", "", path)
            final = re.sub(r"[LR].*", "", partial)
            self.subject_correlated.append(int(final))

        self.iris_imgs = self.load(iris_path)

    def pre_processing(self, img):
        ret, thresh_img = cv2.threshold(img, 30, 200, cv2.THRESH_BINARY_INV)

        comp_img = thresh_img.copy()

        mask_img = np.zeros((self._rows + 2, self._columns + 2), np.uint8)
        cv2.floodFill(comp_img, mask_img, (50, 50), 255)

        bitwise_mask = cv2.bitwise_not(comp_img)

        im_out = thresh_img | bitwise_mask
        pre_img = cv2.morphologyEx(im_out, cv2.MORPH_OPEN,
                                   kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)),
                                   iterations=4)

        norm_img = cv2.bitwise_not(pre_img)
        norm_img = cv2.bitwise_and(img, img, mask=norm_img)

        return pre_img, norm_img

    @staticmethod
    def find_pupil(mask_img):
        kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
        canny_img = cv2.Canny(mask_img, 10, 50)
        dilate_img = cv2.dilate(canny_img, kernel, iterations=1)
        circles_arr = cv2.HoughCircles(dilate_img, cv2.HOUGH_GRADIENT, 1, 20, param1=10, param2=25,
                                       minRadius=20, maxRadius=70)
        return circles_arr

    @staticmethod
    def points_in_circum(radius, n=360):
        return [(np.cos(2 * np.pi / n * x) * radius, np.sin(2 * np.pi / n * x) * radius) for x in xrange(0, n)]

    @staticmethod
    def find_iris(pupil, dimension):
        i = pupil[0, 0]
        radius = i[2] + dimension[0]

        return i[0], i[1], radius

    def find_iris_2(self, img, pupil):
        equ_sqr = img

        i = pupil[0, 0]

        Y_points_old = None
        X_points_old = None
        vector_diff = []
        for r in range(i[2] + 30, i[2] + 80, 1):
            # Dica, pegar a diferenca ponto a ponto, somar o valor absoluto das diferencas, pegar o ponto de maior variacao

            points = self.points_in_circum(r)
            points = np.int16(np.around(points))

            X_points = i[0] + points[:, 0]
            Y_points = i[1] + points[:, 1]

            len_x_lower = sum(X_points < 0)
            len_x_upper = sum(X_points >= self._columns)
            len_y_lower = sum(Y_points < 0)
            len_y_upper = sum(Y_points >= self._rows)

            if len_x_lower > 0 or len_x_upper > 0 or len_y_lower > 0 or len_y_upper > 0:
                print("Out of bounds.")
                break

            if Y_points_old is not None and X_points_old is not None:
                diff_points = equ_sqr[Y_points, X_points] - equ_sqr[Y_points_old, X_points_old]
                diff_abs = np.abs(diff_points)
                diff_sum = np.sum(diff_abs)
                vector_diff.append(diff_sum)

            X_points_old = np.copy(X_points)
            Y_points_old = np.copy(Y_points)

        np_diff = np.asarray(vector_diff)
        max_value = np.argmax(np_diff)

        radius = i[2] + 30 + (max_value * 1 + 1)
        cv2.circle(equ_sqr, (i[0], i[1]), radius, (0, 0, 255), 1)

        return i[0], i[1], radius

    def normalize_iris(self, img, pupil, iris, dimension):
        # Site: https://github.com/frankcorneliusmartin/DaugmanRubberSheet/blob/master/rubberSheetNormalization/rubberSheetNormalisation.m

        rows = dimension[0]
        columns = dimension[1]
        final_img = np.zeros(shape=(rows, columns))

        for radius in range(pupil[2], iris[2]):
            points = self.points_in_circum(radius, columns)
            points = np.int16(np.around(points))

            X_points = pupil[0] + points[:, 0]
            Y_points = pupil[1] + points[:, 1]

            final_img[radius - pupil[2]] = img[Y_points, X_points]

        normalized_img = np.array(final_img)

        return normalized_img

    def normalize_iris_2(self, img, pupil, iris, dimension):

        rows = dimension[0]
        columns = dimension[1]
        max_dimension = rows if iris[2] - pupil[2] >= rows else iris[2] - pupil[2]
        final_img = np.zeros(shape=(rows, columns))

        for radius in range(pupil[2], pupil[2] + max_dimension):
            points = self.points_in_circum(radius, columns)
            points = np.int16(np.around(points))

            X_points = pupil[0] + points[:, 0]
            Y_points = pupil[1] + points[:, 1]

            final_img[radius - pupil[2]] = img[Y_points, X_points]

        normalized_img = np.array(final_img)
        normalized_img = cv2.resize(normalized_img, (dimension[1], dimension[0]), interpolation=cv2.INTER_CUBIC)
        return normalized_img


    def iris_segmentation(self):
        # Criar lista subject que sera usada para comparar com o objeto a ser medido

        img_counter = 0
        img_not_found = 0
        self.normalized_images = []
        self.subject_indexes = []
        dimension = (45, 360)

        for img_index in range(0, len(self.iris_imgs)):
            sample_img = self.iris_imgs[img_index]
            mask_img, norm_img = self.pre_processing(sample_img)
            circles_arr = self.find_pupil(mask_img)

            if circles_arr is not None:
                circles_arr = np.uint16(np.around(circles_arr))
                equ_img = cv2.equalizeHist(norm_img.reshape(self._rows, self._columns))
                iris_arr = self.find_iris(circles_arr, dimension)
                # resh_sample_img = equ_img.copy()
                # iris_arr = self.find_iris_2(equ_img, circles_arr)

                # for i in circles_arr[0, :]:
                #     # print("Circle found: X = {0} Y = {1} Raio = {2}".format(i[0], i[1], i[2]))
                #     cv2.circle(resh_sample_img, (i[0], i[1]), i[2], (255, 0, 0), 2)
                #     cv2.circle(resh_sample_img, (i[0], i[1]), 2, (0, 0, 255), 3)
                #     cv2.circle(resh_sample_img, (i[0], i[1]), iris_arr[2], (255, 0, 0), 2)

                # plt.imshow(resh_sample_img, cmap=plt.cm.gray)
                # plt.show()

                normalized_img = self.normalize_iris(equ_img, circles_arr[0, 0], iris_arr, dimension)
                # normalized_img = self.normalize_iris_2(equ_img, circles_arr[0, 0], iris_arr, dimension)
                self.normalized_images.append(normalized_img)
                self.subject_indexes.append(img_counter)
            else:
                img_not_found += 1

            img_counter += 1

        print("Circles not found: {}".format(img_not_found))
