#!/usr/bin/env python

import getopt
import sys
import time
from Casia import CasiaDB
from Ubiris import UbirisDB


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "d:wl")
    except getopt.GetoptError:
        print('Valid options: -d <casia> -w -l')
        print('-d <casia> valid databases')
        print('-w wavelet verification ')
        print('-l lbp validation')
        sys.exit(1)

    database = 'casia'
    calc_wavelet = False
    calc_Lbp = False

    for opt, arg in opts:
        if opt == '-d':
            database = arg
        if opt == '-w':
            calc_wavelet = True
        if opt == '-l':
            calc_Lbp = True

    iris_recognition = UbirisDB() if database == 'ubiris' else CasiaDB()
    print("Loading {} database ...".format(database))
    iris_recognition.load_base(rule='/*/L/*.jpg')
    start_time = time.time()
    print("Iris segmentation ...")
    iris_recognition.iris_segmentation()
    print("Elapsed Time since iris segmentation start: {}".format(time.time() - start_time))

    if calc_wavelet:
        print("Processing Wavelets ...")
        iris_recognition.wavelet()
        iris_recognition.verification()
        print("Plotting DET Curve ...")
        iris_recognition.plot_det()

    if calc_Lbp:
        print("Processing LBP ...")
        iris_recognition.lbp()
        print("Calculating SVM accuracy score ...")
        iris_recognition.identification_svm()

if __name__ == '__main__':
    main(sys.argv[1:])
