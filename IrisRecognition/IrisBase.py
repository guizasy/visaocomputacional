from operator import itemgetter
import cv2
import matplotlib.pyplot as plt
import numpy as np
import pywt
from skimage.feature import local_binary_pattern
from sklearn.svm import SVC
from sklearn import cross_validation


class IrisBase(object):
    def __init__(self):
        self.iris_imgs = []
        self.normalized_images = []
        self.list_coeffs = []
        self.subject_correlated = []
        self.subject_indexes = []
        self.points = []
        self.list_lbp = []
        self._columns = 100
        self._rows = 100

    def load(self, imgs_path):
        return np.array([cv2.imread(i, 0) for i in imgs_path])

    def print_base(self):
        print('Images: {0}'.format(self.iris_imgs))

    @staticmethod
    def hamming_distance(a, b):
        return np.count_nonzero(a != b)

    def plot_det(self):
        # FAR esta na posicao 1
        # sorted(self.points, key = lambda point: point[1])

        err = 0
        for idx_point in range(len(self.points)):
            if self.points[idx_point][0] > self.points[idx_point][1]:
                X1 = self.points[idx_point - 1][1]
                Y1 = self.points[idx_point - 1][0]
                X2 = self.points[idx_point][1]
                Y2 = self.points[idx_point][0]

                err = (Y1 * X2 - Y2 * X1) / (Y1 - Y2 - X1 + X2)
                break

        print("ERR: {}%".format(err))

        points = np.array(self.points)
        plt.plot(points[:, 0], points[:, 1], 'k')
        plt.xlabel("FAR")
        plt.ylabel("FRR")
        plt.show()

    def wavelet(self):
        """
        Verificacao com metade das amostras do individuo?
        """
        self.list_coeffs = []
        for img in self.normalized_images:
            coeffs = pywt.wavedec2(img, 'haar', level=4)
            cA4, (cH4, cV4, cD4), (cH3, cV3, cD3), (cH2, cV2, cD2), (cH1, cV1, cD1) = coeffs
            soma = cH4 + cV4 + cD4
            soma[soma > 0] = 1
            soma[soma <= 0] = 0
            soma = soma.flatten()

            self.list_coeffs.append(soma)

    def verification(self):
        for min_accept in range(len(np.asarray(self.list_coeffs[0]).flatten())):
            false_acceptation_rate = 0
            false_rejection_rate = 0

            frr_coef = 0
            far_coef = 0
            for idx_test in range(len(self.list_coeffs)):
                for idx_train in range(len(self.list_coeffs)):
                    if idx_train == idx_test:
                        continue

                    dist = self.hamming_distance(self.list_coeffs[idx_test], self.list_coeffs[idx_train])

                    real_subject = self.subject_correlated[self.subject_indexes[idx_test]]
                    veri_subject = self.subject_correlated[self.subject_indexes[idx_train]]

                    # False Acceptance Rate
                    if veri_subject != real_subject:
                        far_coef += 1
                        if dist <= min_accept:
                            false_acceptation_rate += 1

                    # False Rejection Rate
                    if veri_subject == real_subject:
                        frr_coef += 1
                        if dist > min_accept:
                            false_rejection_rate += 1

            # calcula FRR %
            frr = (float(false_rejection_rate) / frr_coef) * 100
            # calcula FRR %
            far = (float(false_acceptation_rate) / far_coef) * 100

            self.points.append([far, frr])

    def lbp(self):
        """
        SVM - Testar a identificacao com 80/20
        """

        radius = 1
        n_points = 8 * radius
        method = 'default'
        self.list_lbp = []

        for img in self.normalized_images:
            lbp_img = local_binary_pattern(img, n_points, radius, method=method)
            lbp_hist, bins = np.histogram(lbp_img.ravel(), 256, [0, 256])
            self.list_lbp.append(lbp_hist)


    def identification_svm(self):
        subjects = itemgetter(*self.subject_indexes)(self.subject_correlated)
        clf = SVC(kernel='linear')
        score = cross_validation.cross_val_score(clf, self.list_lbp, subjects, cv=5)
        print("SVM Accuracy Score: {}".format(np.mean(score) * 100))
