import glob
import logging
import numpy as np
import re
import pedestrian_db
import os

from skimage.feature import hog


class Cbcl(pedestrian_db.PedestrianDB):
    def __init__(self, save_output=False):
        super(Cbcl, self).__init__()

        self.rows = 128
        self.columns = 64
        self.save_output = save_output

        self.winH = 128
        self.winW = 64

        # create logger
        self.logger = logging.getLogger('Cbcl')
        self.logger.info('creating an instance of CBCL class')

        np.set_printoptions(precision=4, linewidth=200, suppress=True)

    def load_database(self, database_dict):
        path = database_dict['path']
        rule = database_dict['rule']
        annotation_path = database_dict['annotation']

        self.logger.info('Loading database path: {}'.format(path))

        images_path = glob.glob(path)
        images_path = [x for x in images_path if x.rfind('Thumbs') == -1]
        images = self.load(images_path)

        subjects = []
        for im_path in images_path:
            partial = os.path.basename(im_path)
            final = re.sub(r"{}".format(rule), "", partial)
            subjects.append(final)

        self.logger.info("Subjects: {}".format(subjects))

        annotations = []
        if annotation_path != '':
            path = self.load_database_anottation_window(annotation_path)
            annotations.append(path)

        self.logger.info("Annotations: {}".format(annotations))

        image_dictionary = {'images': images, 'subjects': subjects, 'annotations': annotations}

        return image_dictionary

    def load_database_anottation_window(self, path='./databases/INRIAPerson/Test/annotations/*.txt'):
        self.logger.info("Images INRIA POS annotations")

        annotations_path = glob.glob(path)
        self.logger.info("Annotations_path: {}".format(path))

        annot_inria_pos = []
        for a in annotations_path:
            # print "\n", a

            with open(a) as f:
                data = f.read()
                objs = re.findall('\(\d+, \d+\)[\s\-]+\(\d+, \d+\)', data)

                # Load object bounding boxes into a data frame.
                annot_image = []
                for ix, obj in enumerate(objs):
                    coor = re.findall('\d+', obj)
                    col1 = float(coor[0])
                    row1 = float(coor[1])
                    col2 = float(coor[2])
                    row2 = float(coor[3])

                    column = col2 - col1
                    row = row2 - row1
                    coord = [row1, col1, row2, col2, row, column]

                    annot_image.append(coord)

                annot_inria_pos.append(annot_image)

                self.logger.info("Boxes: {}".format(annot_inria_pos))

        return annot_inria_pos

    def crop_roi(self, images):
        cropped_image = []

        for idx in range(len(images)):
            image = images[idx]

            if image.shape[0] > self.rows  and image.shape[1] > self.columns:
                r = (image.shape[0] - self.rows) / 2
                c = (image.shape[1] - self.columns) / 2
                # TODO: Check image size
                cropped_image.append(image[r:r + self.rows, c:c + self.columns])
            else:
                cropped_image.append(image)

        return cropped_image

    def myhog(self, window):
        kernel = np.array([[0, 0, 0], [-1, 0, 1], [0, 0, 0]])
        gx = self.gradient_image(window, kernel)
        gy = self.gradient_image(window, kernel.T)

        # self.logger.info("Gx.Shape: {} Gx: {}".format(gx.shape, gx))
        # self.logger.info("Gy.Shape: {} Gy: {}".format(gy.shape, gy))

        gx_max = np.amax(gx, axis=2)
        gy_max = np.amax(gy, axis=2)

        # self.logger.info("GxMax.Shape: {} GxMax: {}".format(gx_max.shape, gx_max))
        # self.logger.info("GyMax.Shape: {} GyMax: {}".format(gy_max.shape, gy_max))

        orientation = np.arctan2(gy_max, gx_max)
        magnitude = np.sqrt(np.power(gx_max, 2) + np.power(gy_max, 2))
        positive_orientation = np.where(orientation < 0, orientation + np.pi, orientation)
        # self.logger.info("Magnitude: {} Orientation: {}".format(magnitude, converted_orientation))

        histogram = self.compute_histogram(positive_orientation, magnitude)
        hog_descriptor = self.create_blocks(histogram)

        return hog_descriptor

    def skhog(self, image):
        fd = hog(image, orientations=9, pixels_per_cell=(8, 8), transform_sqrt=True,
                 cells_per_block=(2, 2), visualise=False)

        # fd, hog_image = hog(image, orientations=9, pixels_per_cell=(16, 16), transform_sqrt=False,
        #                     cells_per_block=(1, 1), visualise=True)
        #
        # fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4), sharex=True, sharey=True)
        #
        # ax1.axis('off')
        # ax1.imshow(image, cmap=plt.cm.gray)
        # ax1.set_title('Input image')
        # ax1.set_adjustable('box-forced')
        #
        # # Rescale histogram for better display
        # hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.02))
        #
        # ax2.axis('off')
        # ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
        # ax2.set_title('Histogram of Oriented Gradients')
        # ax1.set_adjustable('box-forced')
        # plt.show()

        # print("Hog: {} Hog.Size: {}".format(fd, fd.shape))

        return fd

    def calculate_hog(self, image_dictionary, stepSize=8, imageCounter=0, returnBoxes=False):
        images = image_dictionary['images']
        subjects = image_dictionary['subjects']
        bboxes = image_dictionary['annotations']
        bbox_info = []

        counter = 0
        descriptor = []

        for idx in range(len(images)):
            image = images[idx]
            subject = subjects[idx]
            bbox = None if not bboxes else bboxes[idx]

            image = self.gamma_correction(image)
            pyramid = self.pyramid(image, subject, bbox)
            for idx_pyr in range(len(pyramid)):
                for (column, row, window) in self.sliding_window(pyramid[idx_pyr][0], stepSize=stepSize,
                                                                 windowSize=(self.rows, self.columns)):

                    # if the window does not meet our desired window size, ignore it
                    if window.shape[0] != self.winH or window.shape[1] != self.winW:
                        continue

                    if imageCounter > 0 and counter >= imageCounter:
                        return descriptor

                    if returnBoxes:
                        bbox_info.append([row, column, pyramid[idx_pyr][3]])

                    counter += 1
                    # descriptor.append(self.myhog(window))
                    descriptor.append(self.skhog(window))

        descriptor = np.asarray(descriptor)

        if returnBoxes:
            return descriptor, bbox_info
        else:
            return descriptor

