import cv2
import numpy as np
import logging
import os


class PedestrianDB(object):
    def __init__(self):
        self.database_paths = []
        # self.images = []
        # self.subjects = []

        self.rows = 128
        self.columns = 64

        self.dict_rescale = {}

        # create logger
        self.logger = logging.getLogger('PedestrianDB')
        self.logger.info('creating an instance of PedestrianDB class')

    def load(self, path):
        self.logger.info('Loading databases path: {}'.format(path))
        # images = np.array([cv2.imread(i) for i in path])
        images = np.array([cv2.imread(i, 0) for i in path])

        return images

    def save(self, images, subjects, output_path='./output/image/'):
        self.logger.info("Saving images in: {}".format(output_path))

        if not os.path.exists(output_path):
            os.makedirs(output_path)

        for idx in range(len(images)):
            cv2.imwrite("{}{}.png".format(output_path, subjects[idx]), images[idx])

    def save_single(self, image, subject, output_path='./output/image/'):
        if not os.path.exists(output_path):
            os.makedirs(output_path)

        print("Image Shape: {} Subject: {}".format(image.shape, subject))

        cv2.imwrite("{}{}.png".format(output_path, subject), image)

    @staticmethod
    def gamma_correction(img, correction=0.5):
        img_b = img / 255.0
        img_b = cv2.pow(img_b, correction)
        return np.uint8(img_b * 255)

    @staticmethod
    def sliding_window(image, stepSize, windowSize):
        # slide a window across the image
        windows_slide = []

        for row in xrange(0, image.shape[0] - windowSize[0] + 1, stepSize):
            for column in xrange(0, image.shape[1] - windowSize[1] + 1, stepSize):
                # yield the current window
                # yield np.asarray([column, row, image[row:row + windowSize[0], column:column + windowSize[1]]])
                windows_slide.append((column, row, image[row:row + windowSize[0], column:column + windowSize[1]]))

        return windows_slide

    def pyramid(self, image, subject, bbox=None, scale=0.9, minSize=(128, 64)):
        self.logger.info("Rescaling image function")

        img = image.copy()
        idx = 0
        dict_rimage = {idx: [img, bbox, subject, 1.0]}
        while True:
            self.logger.info("Scale[0]: {} Scale[1]: {}".format(img.shape[0], img.shape[1]))
            img = cv2.resize(img, None, fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)

            if img.shape[0] < minSize[0] or img.shape[1] < minSize[1]:
                break

            if bbox is not None:
                bbox = np.multiply(bbox, scale)

            new_subject = "{}_{}x{}".format(subject, img.shape[0], img.shape[1])

            idx += 1
            dict_rimage[idx] = [img, bbox, new_subject, (1.0 * image.shape[0]) / img.shape[0]]

        self.logger.info("Dictionary Len: {}".format(len(dict_rimage)))
        return dict_rimage

    @staticmethod
    def gradient_image(image, kernel):
        return np.asarray(cv2.filter2D(image, -1, kernel), dtype=np.float32)

    @staticmethod
    def get_histogram(self, magnitude, orientation, bins=9):
        binSize = np.pi / bins
        minAngle = 0
        # self.logger.info("BinSize: {}".format(binSize))

        before_round = (orientation - minAngle) / binSize
        leftBinIndex = np.round(before_round)
        # leftBinIndex = np.round((orientation - minAngle) / binSize)
        rightBinIndex = leftBinIndex + 1

        leftBinCenter = ((leftBinIndex - 0.5) * binSize) - minAngle

        rightPortions = orientation - leftBinCenter
        leftPortions = binSize - rightPortions
        rightPortions = rightPortions / binSize
        leftPortions = leftPortions / binSize

        # self.logger.info("LeftBinIndex: {}".format(leftBinIndex))
        # self.logger.info("CenterBinIndex: {}".format(leftBinCenter))
        # self.logger.info("RigthBinIndex: {}".format(rightBinIndex))

        leftBinIndex[leftBinIndex == 0] = bins - 1
        rightBinIndex[rightBinIndex == bins] = 0

        hist = np.zeros(bins)

        for i in range(bins):
            pixels = np.where(leftBinIndex == i)

            # print("Pixels: {}".format(pixels))
            tmpVal = leftPortions[pixels[0], pixels[1]]
            tmpMag = magnitude[pixels[0], pixels[1]]
            hist[i] += np.sum(tmpVal.T * tmpMag)

            pixels = np.where(rightBinIndex == i)
            tmpVal = rightPortions[pixels[0], pixels[1]]
            tmpMag = magnitude[pixels[0], pixels[1]]
            hist[i] += np.sum(tmpVal.T * tmpMag)

        # self.logger.info("Histogram: {}".format(hist))
        return hist

    def compute_histogram(self, orientation, magnitude):
        window_size_r = window_size_c = 8
        histogram = np.zeros((16, 8, 9))

        for r in range(0, (self.rows - window_size_r) + 1, window_size_r):
            for c in range(0, (self.columns - window_size_c) + 1, window_size_c):
                cellAngles = orientation[r:r + window_size_r, c:c + window_size_c]
                cellMagnitudes = magnitude[r:r + window_size_r, c:c + window_size_c]
                histogram[r / window_size_r, c / window_size_c, :] = (self.get_histogram(cellMagnitudes, cellAngles))
                # histogram.append(self.get_histogram(cellMagnitudes, cellAngles))
                # binSize = np.pi / 9
                # minAngle = 0
                # histogram[r / window_size_r, c / window_size_c, :] = (get_histogram(cellMagnitudes, cellAngles, np.round_((orientation - minAngle) / binSize)))
                # histogram[r / window_size_r, c / window_size_c, :] = (get_histogram(cellMagnitudes, cellAngles))

        # self.logger.info("Hist Shape: {}".format(histogram.shape))
        return histogram

    def create_blocks(self, matrix, block_size=2):
        window_size_r = block_size
        window_size_c = block_size

        descriptor = []
        for r in range(0, (matrix.shape[0] - window_size_r) + 1, window_size_r / 2):
            for c in range(0, (matrix.shape[1] - window_size_c) + 1, window_size_c / 2):
                block = matrix[r:r + window_size_r, c:c + window_size_c]
                magnitude = np.linalg.norm(block) + 0.01
                normalized = block / magnitude
                normalized = normalized.ravel()
                descriptor.append(normalized)

        descriptor = np.asarray(descriptor).ravel()
        self.logger.info("Descriptor Shape: {} Descriptor: {}".format(np.asarray(descriptor).shape, descriptor))

        return descriptor

    @staticmethod
    def overlap_squares(inRect, compRect, threshold=0.3):
        inR = inRect[0] * inRect[4]
        inC = inRect[1] * inRect[4]
        enR = (inR + inRect[2]) * inRect[4]
        enC = (inC + inRect[3]) * inRect[4]

        inArea = (inRect[2] - inRect[0]) * (inRect[3] - inRect[1])

        values = []
        for idx in range(len(compRect)):
            ouR = compRect[idx][0]
            ouC = compRect[idx][1]
            onR = compRect[idx][2]
            onC = compRect[idx][3]

            outArea = compRect[idx][4] * compRect[idx][5]

            row_overlap = np.max(0, min(enR, onR) - max(inR, ouR))
            column_overlap =  np.max(0, min(enC, onC) - max(inC, ouC))

            intersectArea = row_overlap * column_overlap
            unionArea = inArea + outArea - intersectArea
            values.append(1.0 * intersectArea / unionArea)

        return values

    # Malisiewicz et al.
    @staticmethod
    def non_max_suppression_fast(annotations, overlapThresh=0.3):
        # if there are no boxes, return an empty list
        if len(annotations) == 0:
            return []

        # if the bounding boxes integers, convert them to floats --
        # this is important since we'll be doing a bunch of divisions
        if annotations.dtype.kind == "i":
            annotations = annotations.astype("float")

        # grab the coordinates of the bounding boxes
        r1 = (annotations[:, 0]) * annotations[:, 2]
        c1 = (annotations[:, 1]) * annotations[:, 2]
        r2 = (annotations[:, 0] + 128) * annotations[:, 2]
        c2 = (annotations[:, 1] +  64) * annotations[:, 2]

        area = (r2 - r1 + 1) * (c2 - c1 + 1)
        idxs = np.arange(len(annotations))

        # initialize the list of picked indexes
        pick = []

        # keep looping while some indexes still remain in the indexes
        # list
        while len(idxs) > 0:
            # grab the last index in the indexes list and add the
            # index value to the list of picked indexes
            last = len(idxs) - 1
            i = idxs[last]
            pick.append(i)

            # find the largest (x, y) coordinates for the start of
            # the bounding box and the smallest (x, y) coordinates
            # for the end of the bounding box
            rr1 = np.maximum(r1[i], r1[idxs[:last]])
            cc1 = np.maximum(c1[i], c1[idxs[:last]])
            rr2 = np.minimum(r2[i], r2[idxs[:last]])
            cc2 = np.minimum(c2[i], c2[idxs[:last]])

            # compute the width and height of the bounding box
            w = np.maximum(0, cc2 - cc1 + 1)
            h = np.maximum(0, rr2 - rr1 + 1)

            # compute the ratio of overlap
            overlap_1 = (w * h) / area[idxs[:last]]
            overlap_2 = area[idxs[:last]] / (w * h)
            out = np.isfinite(overlap_2)
            overlap_2[np.where(out == False)] = 0

            overlap = np.maximum(overlap_1, overlap_2)

            # delete all indexes from the index list that have
            to_remove = np.concatenate(([last], np.where(overlap > overlapThresh)[0]))
            idxs = np.delete(idxs, to_remove)

        # return only the bounding boxes that were picked using the
        # integer data type
        return pick

    def draw_rects(self, img, annotations, basename='1.png'):
        for info in annotations:
            pt1 = (int(info[1] * info[2]), int(info[0] * info[2]))
            pt2 = (int((info[1] + 64) * info[2]), int((info[0] + 128) * info[2]))
            cv2.rectangle(img, pt1, pt2, 255)

        self.save_single(img, basename, output_path='./output/bboxes/')
