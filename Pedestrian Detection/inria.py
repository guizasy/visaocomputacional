import glob
import logging
import numpy as np
import re
import pedestrian_db


class InriaDB(pedestrian_db.PedestrianDB):
    def __init__(self, save_output=False):
        super(InriaDB, self).__init__()

        self.rows = 320
        self.columns = 240
        self.save_output = save_output

        # create logger
        self.logger = logging.getLogger('Inria')
        self.logger.info('creating an instance of INRIA class')

        np.set_printoptions(precision=4, linewidth=200, suppress=True)

    def load_database(self, path='./databases/INRIAPerson/Test/*/*'):
        self.logger.info('Images path: {}'.format(path))

        images_path = glob.glob(path)
        self.load(images_path)

        for path in images_path:
            partial = re.sub(r".*Test\\", "", path)
            status = re.sub(r"(.*)\.*", "", partial)
            final = re.sub(r"/.*", "", partial)
            self.logger.info("Partial: {} Subjects: {} Status: {}".format(partial, final, status))
            self.subjects.append(final)

        # self.logger.info("Subjects: {}".format(self.subjects))

    def run(self):
        pass
        # self.pyramid()

        # if self.save_output:
        #     for idx in range(self.dict_rescale):
        #
        #         self.save(self.dict_rescale, output_path='./output/original/')