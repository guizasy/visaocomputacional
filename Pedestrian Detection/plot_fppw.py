import numpy as np
import os
import pickle
import matplotlib.pyplot as plt

def load_obj(name):
    if os.path.isfile('obj/'+ name + '.pkl'):
        with open('obj/' + name + '.pkl', 'rb') as f:
            return pickle.load(f)
    return None

def plot_msvfppw(dict_hogs):
    decision_function = []
    labels = []
    windows = 0

    for key, value in dict_hogs.items():
        for k, v in value.items():
            if k == 'windows_counter':
                windows += value
            elif k == 'decision_func':
                decision_function.extend(value['decision_func'])
            elif k == 'labels':
                labels.extend(value['labels'])

    decision_function = np.asarray(decision_function)
    labels = np.asarray(labels)

    print("DF: {} L: {}".format(decision_function, labels))
    maximum = np.amax(decision_function)
    minimum = np.amin(decision_function)
    step = -((maximum - minimum) / 100.0)
    print("Maximum: {}".format(maximum))
    print("Minimum: {}".format(minimum))
    print("Step: {}".format(step))

    missrate = []
    fppw = []
    allPositive = len(np.where(labels)[0])
    print("allPositive: {}".format(allPositive))

    for threshold in np.arange(maximum, minimum + step, step):
        fn_idx = np.where(labels == True)
        idx = np.where(decision_function[fn_idx[0]] < threshold)
        falseNegative = len(idx[0])

        fp_idx = np.where(labels == False)
        idx = np.where(decision_function[fp_idx[0]] >= threshold)
        falsePositive = len(idx[0])

        missrate.append(1.0 * falseNegative / allPositive)
        fppw.append(1.0 * falsePositive / windows)

    fig = plt.figure()
    fig.suptitle("Curva", fontsize=18, fontweight='bold')
    ax = fig.add_subplot(111)
    ax.set_title("Missrate vs. FPPW")
    ax.set_xlabel('FPPW')
    ax.set_ylabel('Missrate')
    ax.semilogx(fppw, missrate, linestyle='--', marker='o', color='b')
    fig.savefig("./curve.png")

obj = load_obj('test_positive')
plot_msvfppw(obj)
