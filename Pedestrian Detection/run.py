#!/usr/bin/env python

import argparse
import logging
import sys
import os.path
import pickle
import numpy as np
import time

from sklearn import svm
from cbcl import Cbcl

from collections import namedtuple
Rectangle = namedtuple('Rectangle', 'rmin cmin rmax cmax')

_log_level = {
    'DEBUG': logging.DEBUG,
    'ERROR': logging.ERROR,
    'WARNING': logging.WARNING,
    'INFO': logging.INFO
}

database_entries = {
    'train': {
        'mit': {
            'path': './databases/pedestrians128x64/*.ppm',
            'annotation': '',
            'rule': '.ppm',
            'type': 'positive'
        },
        'inria_pos': {
            'path': './databases/INRIAPerson/train_64x128_H96/pos/*.png',
            'annotation': '',
            'rule': '(.jpg)|(.png)',
            'type': 'positive'
        },
        'inria_neg': {
            'path': './databases/INRIAPerson/Train/neg/*',
            'annotation': '',
            'rule': '(.jpg)|(.png)',
            'type': 'negative'
        }
    },
    'test': {
        'inria_pos': {
            'path': './databases/INRIAPerson/Test/pos/*',
            'annotation': './databases/INRIAPerson/Test/annotations/*.txt',
            'rule': '(.jpg)|(.png)',
            'type': 'positive'
        # },
        # 'inria_neg': {
        #     'path': './databases/INRIAPerson/Test/neg/*',
        #     'annotation': '',
        #     'rule': '.(.jpg)|.(.png)',
        #     'type': 'negative'
        }
    }
}

def save_obj(name, obj):
    if not os.path.exists('./obj'):
        os.makedirs('./obj')

    with open('obj/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    if os.path.isfile('obj/'+ name + '.pkl'):
        with open('obj/' + name + '.pkl', 'rb') as f:
            return pickle.load(f)
    return None

def configure_logging(args):
    # create logger with 'fingerprint_application'
    root = logging.getLogger()
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    root.setLevel(_log_level[args.log_level])

    # create file handler which logs even debug messages
    if args.output_log_file != '':
        fh = logging.FileHandler(args.output_log_file, 'w')
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        root.addHandler(fh)

    # create console handler with a higher log level
    if not args.quiet:
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)
        root.addHandler(ch)

def create_feature_label(negative_train_hog, positive_train_hog):
    print ("Negative Shape: {} Positive Shape: {}".format(negative_train_hog.shape, positive_train_hog.shape))

    negative_labels = np.zeros(negative_train_hog.shape[0])
    positive_labels = np.ones(positive_train_hog.shape[0])

    labels = np.concatenate((negative_labels, positive_labels))
    features = np.concatenate((negative_train_hog, positive_train_hog))

    return features, labels

def area(a, b):  # returns None if rectangles don't intersect
    dx = min(a.rmax, b.rmax) - max(a.rmin, b.rmin)
    dy = min(a.cmax, b.cmax) - max(a.cmin, b.cmin)

    area_a = (a.rmax - a.rmin) * (a.cmax - a.cmin)
    area_b = (b.rmax - b.rmin) * (b.cmax - b.cmin)

    if area_a < area_b:
        div_area = area_a
    else:
        div_area = area_b

    if div_area <= 0.0:
        print("div_area={}! ".format(div_area))

    if (dx >= 0) and (dy >= 0) and div_area > 0:
        return dx * dy / div_area
    else:
        return 0.0

def train_svc(train_feature, train_label):
    lin_clf = svm.LinearSVC()
    lin_clf.fit(train_feature, train_label)

    return lin_clf

def run(args):
    database = Cbcl(args.save_output)

    negative_train_hog = None
    positive_train_hog = None
    hard_negative_hog = None
    # negative_test_hog = None
    positive_test_hog = None

    if args.load_dictionary:
        negative_train_hog = load_obj('train_negative')
        positive_train_hog = load_obj('train_positive')
        hard_negative_hog = load_obj('train_hard')
        # negative_test_hog = load_obj('test_negative')
        positive_test_hog = load_obj('test_positive')

    negative_train_dict = {}
    positive_train_dict = {}
    for key, value in database_entries['train'].items():
        # print("Database Name: {} Values: {}".format(key, value))

        if value['type'] == 'negative':
            negative_train_dict[key] = database.load_database(value)
        elif value['type'] == 'positive':
            positive_train_dict[key] = database.load_database(value)

    if negative_train_hog is None:
        negative_train_hog = []

        for key, value in negative_train_dict.items():
            descriptor = database.calculate_hog(value, stepSize=128, imageCounter=30000)
            negative_train_hog.extend(descriptor)

        negative_train_hog = np.asarray(negative_train_hog)

        if (negative_train_hog.size):
            print("Negative Image: {} Shape: {}".format(negative_train_hog, negative_train_hog.shape))
            save_obj('train_negative', negative_train_hog)

    if positive_train_hog is None:
        positive_train_hog = []

        for key, value in positive_train_dict.items():
            value['images'] = database.crop_roi(value['images'])
            descriptor = database.calculate_hog(value)
            positive_train_hog.extend(descriptor)

        positive_train_hog = np.asarray(positive_train_hog)

        if (positive_train_hog.size):
            print("Positive Image: {} Shape: {}".format(positive_train_hog, positive_train_hog.shape))
            save_obj('train_positive', positive_train_hog)

    feature_train, label_train = create_feature_label(negative_train_hog, positive_train_hog)
    print("Feature Shape: {} Labels Shape: {}".format(feature_train.shape, label_train.shape))

    if hard_negative_hog is None:
        hard_negative_hog = []

        clf = train_svc(feature_train, label_train)

        for key, value in negative_train_dict.items():
            for idx in range(len(value['images'])):
                dict_one = {'images': [value['images'][idx]], 'subjects': [value['subjects'][idx]], 'annotations': []}

                descriptor = database.calculate_hog(dict_one, stepSize=8)

                labels = clf.predict(descriptor)
                positive_labels = [i for i, v in enumerate(labels) if v > 0]

                if len(positive_labels):
                    hard_negative_hog.extend(descriptor[positive_labels])

                # TODO: Put this code in functions and change break to a return condition only work for 1 element
                # TODO: Leave all negative train images be classified wrong
                # if len(hard_negative_hog) > 10000:
                #     break

        hard_negative_hog = np.asarray(hard_negative_hog)

        if (hard_negative_hog.size):
            print("HNM Image: {} Shape: {}".format(hard_negative_hog, hard_negative_hog.shape))
            save_obj('train_hard', hard_negative_hog)

    print("Create SVM Groups")
    feature_train, label_train = create_feature_label(np.concatenate((negative_train_hog, hard_negative_hog)), positive_train_hog)
    print("Training SVM")
    clf = train_svc(feature_train, label_train)

    print("Load positive INRIA database")
    positive_test_dict = {}
    for key, value in database_entries['test'].items():
        print("Database Name: {} Values: {}".format(key, value))

        if value['type'] == 'positive':
            positive_test_dict[key] = database.load_database(value)

    print("Calculate hog descriptor of positive_test_hog")
    if positive_test_hog is None:
        positive_test_hog = {}

        for key, value in positive_test_dict.items():
            for idx in range(len(value['images'])):
                print("HOG SUBJECT: {}".format(value['subjects'][idx]))
                # print("ANNOTATIONS LINE: {}".format(value['annotations'][0][idx]))
                dict_one = {'images': [value['images'][idx]], 'subjects': [value['subjects'][idx]], 'annotations': [value['annotations'][0][idx]]}
                descriptor, annotations = database.calculate_hog(dict_one, stepSize=8, returnBoxes=True)

                # print("Converting NP Array")
                descriptor = np.asarray(descriptor)
                annotations = np.asarray(annotations)

                print("Predicting Labels")
                labels = clf.predict(descriptor)
                positive_labels = [i for i, v in enumerate(labels) if v > 0]
                positive_test_hog[value['subjects'][idx]] = {}
                positive_test_hog[value['subjects'][idx]]['windows_counter'] = len(labels)
                hogs = []
                annotations_pos = []

                if len(positive_labels) > 0:
                    decision_func = clf.decision_function(descriptor[positive_labels])

                    print("Decision Function: {}".format(decision_func))
                    decision_func = np.absolute(np.asarray(decision_func))
                    sorted_idx = np.argsort(decision_func)

                    hogs = descriptor[positive_labels][sorted_idx]
                    annotations_pos = annotations[positive_labels][sorted_idx]

                    final_idx = database.non_max_suppression_fast(annotations_pos)
                    # positive_test_hog.extend(hogs[final_idx])
                    positive_test_hog[value['subjects'][idx]] = {
                        'hogs': hogs[final_idx],
                        'annotations': annotations_pos[final_idx],
                        'decision_func': decision_func[sorted_idx][final_idx]
                    }

                    database.draw_rects(value['images'][idx], annotations_pos[final_idx], "{}.png".format(value['subjects'][idx]))

                    true_annotations = value['annotations'][0][idx]
                    window_labels = np.zeros(len(hogs[final_idx]), dtype=bool)
                    for ann in true_annotations:
                        for i, ann_pos in enumerate(annotations_pos[final_idx]):
                            # ra = Rectangle(ann[0], ann[1], ann[2], ann[3])
                            ra = Rectangle(ann[0], ann[1], ann[2], ann[3])
                            # rb = Rectangle(ann_pos[0] * ann_pos[2], ann_pos[1] * ann_pos[2], (ann_pos[0] + 128) * ann_pos[2], (ann_pos[1] + 64) * ann_pos[2])
                            rb = Rectangle(ann_pos[0] * ann_pos[2], ann_pos[1] * ann_pos[2], (ann_pos[0] + 128) * ann_pos[2], (ann_pos[1] + 64) * ann_pos[2])
                            if area(ra, rb) >= 0.5:
                                window_labels[i] = True

                    if window_labels.shape[0] > 0:
                        positive_test_hog[value['subjects'][idx]]['labels'] =  window_labels

        # positive_test_hog = np.asarray(positive_test_hog)

        if (len(positive_test_hog)):
            # print("HNM Image: {} Shape: {}".format(positive_test_hog, positive_test_hog.shape))
            # print("HNM Image: {}".format(positive_test_hog))
            save_obj('test_positive', positive_test_hog)

    print("Dict Pos Hog: {}".format(positive_test_hog))

def main(argv):
    parser = argparse.ArgumentParser('python run.py ')
    parser.add_argument('--save_output', '-s', action='store_true', help='save output images of process')
    parser.add_argument('--load_dictionary', '-ld', action='store_true', help='load hog values from a pickle fie')
    parser.add_argument('--log_level', '-l', dest='log_level', help='set log level of application', default='ERROR')
    parser.add_argument('--output_log', '-o', dest='output_log_file', help='output file to be used', default='')
    parser.add_argument("--quiet", "-q", action="store_true", help='quiet console output')
    args = parser.parse_args()

    configure_logging(args)
    logging.debug('Arguments received: {}'.format(args))
    logging.info('creating an instance of Main function')

    start_time = time.time()
    run(args)
    print("Execution time: --- %s seconds ---" % (time.time() - start_time))
    # database = None if args.dbname == "INRIA" else Cbcl(args.save_output)
    # database.run()

if __name__ == '__main__':
    main(sys.argv[1:])